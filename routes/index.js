var express = require('express');
var router = express.Router();

// route for Homepage
router.get('/', ensureAuthenticated, function(req, res){
	res.render('index');
});

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		// if not logged in then redirect to login page
		res.redirect('/users/login');
	}
}

module.exports = router;
